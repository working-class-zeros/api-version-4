var mongoose = require('mongoose');

var AddressJSON = {
    name: String,
    address: String,
    location: String,
    Timing : String
}
var appointmentJSON = {
    patientId: {type:mongoose.Schema.Types.ObjectId, ref:'Patient'},
    doctorId: {type:mongoose.Schema.Types.ObjectId, ref:'Doctor'},
    patientName : String,
    patientContact : String,
    status : String, // Enumify
    doctorName : String,
    doctorSpeciality: String,
    clinicAddress : AddressJSON,
    doctorContact : String,
    time : String,
    date : String,
    reasonForVisit : String

}

var appointmentSchema = new mongoose.Schema(appointmentJSON);
module.exports = mongoose.model('CancelledAppointment', appointmentSchema);