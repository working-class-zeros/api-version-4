var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var AddressJSON = {
    name: String,
    address: String,
    timing: [String],
    location: String
}

var doctorJSON = {
 
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name:{type: String,
          lowercase:true},
    dob: String,
    rating: Number,
    contactNumber: String,
    addresses: [AddressJSON],
    qualifications: [String],
    speciality: String,
    bio:String,
    avgPrice:Number,
    appointment: [{type:mongoose.Schema.Types.ObjectId, ref:'Appointment'}],
    cancelledAppointment : [{type:mongoose.Schema.Types.ObjectId, ref:'CancelledAppointment'}],
    history: [{type:mongoose.Schema.Types.ObjectId, ref:'History'}],
    notifToken: [{
        type:String,
        unique: true
    }]
}
var doctorSchema = new mongoose.Schema(doctorJSON);

doctorSchema.pre('save', function(next){
 
    var user = this;
    var SALT_FACTOR = 5;
 
    if(!user.isModified('password')){
        return next();
    }
 
    bcrypt.genSalt(SALT_FACTOR, function(err, salt){
 
        if(err){
            return next(err);
        }
 
        bcrypt.hash(user.password, salt, null, function(err, hash){
 
            if(err){
                return next(err);
            }
 
            user.password = hash;
            next();
 
        });
 
    });
 
});
 
doctorSchema.methods.comparePassword = function(passwordAttempt, cb){
 
    bcrypt.compare(passwordAttempt, this.password, function(err, isMatch){
 
        if(err){
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });
 
}
 
module.exports = mongoose.model('Doctor', doctorSchema);