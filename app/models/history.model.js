var mongoose = require('mongoose');

var AddressJSON = {
    name: String,
    address: String,
    location: String,
    Timing: String
}

var historyJSON = {
    patientId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Patient'
    },
    doctorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Doctor'
    },
    patientName: String,
    status: String,
    patientContact: String,
    status: String,
    doctorName: String,
    doctorContact: String,
    doctorSpeciality: String,
    clinicAddress: AddressJSON,
    time: String,
    date: String,
    reasonForVisit: String,
    prescription: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Prescription'
    }
}

var historySchema = new mongoose.Schema(historyJSON);
module.exports = mongoose.model('History', historySchema);