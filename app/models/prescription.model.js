const mongoose = require('mongoose');
var medicineJSON = {
    name : String,
    instructions : [String],
    directions : String,
    duration: Number,
    reason: [String]
}
var testJSON = {
    name: String,
    comments: String
}

var AddressJSON = {
    name: String,
    address: String,
    location: String,
    Timing : String
}
var prescriptionJSON = {
    patientId: {type:mongoose.Schema.Types.ObjectId, ref:'Patient'},
    doctorId: {type:mongoose.Schema.Types.ObjectId, ref:'Doctor'},
    doctorName : String,
    doctorQualification : [String],
    clinicAddress : AddressJSON,
    patientName: String,
    patientDOB : String,
    date : String,
    doctorContact : String,
    medicines : [medicineJSON],
    tests : [testJSON],
    status: String,
}

/*
    var prescriptionJSON = {
        patientID : 0813,
        doctorID : 12087423,
        ClinicAddress: []
        medicines : [
            {
                name: aoiher, instructions: "Dont take", howMany:5/day, duration: 1 week, reason: "u dying"
            },
            {
                name: poison, instructions:"Take", howMany:1/day, duration: 2 weeks, reason: "Life sucks"
            }
        ]
        tests: [
            {
                name:"CT Scan", comments: "U need it"
            }
        ]
    }
*/
var prescriptionSchema = new mongoose.Schema(prescriptionJSON);
module.exports = mongoose.model('Prescription', prescriptionSchema);