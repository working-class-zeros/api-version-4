var mongoose = require('mongoose');


var reviewJSON = {
    doctorId : {type:mongoose.Schema.Types.ObjectId, ref:'Doctor'},
    patientId : {type:mongoose.Schema.Types.ObjectId, ref:'Patient'},
    reviewType : String, // Doctor or Patient? Doctor means review is for a doctor
    patientName : String,
    doctorName : String,
    rating: Number,
    review: String
}
var reviewSchema = new mongoose.Schema(reviewJSON)
module.exports = mongoose.model('Review', reviewSchema);