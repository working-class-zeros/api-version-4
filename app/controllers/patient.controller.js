var Patient = require("../models/patient.model");
var Doctor = require("../models/doctor.model");
var Appointment = require('../models/appointment.model');
var History = require('../models/history.model');
var Prescription = require("../models/prescription.model");
var CancelledAppointment = require('../models/appointment-cancel.model');
var fcmController = require('./fcm.controller.js');

// Incomplete
exports.getHistory = async function (req, res, next) {
    var id = req.params.id;
    console.log(id);
    var patient = await Patient.findById(id);
    console.log(patient);
    var history = patient['history'];
    console.log(history);
    var temp = [];
}

exports.searchDoctor = async function (req, res, next) {
    var drName = req.params.name;

    drName = drName.replace("+", " ");
    drName = drName.toLowerCase();
    var doctor = await Doctor.find({
        name: drName
    });
    if (doctor.length == 0) {
        res.json({
            status: 404
        }).status(404);
        return;
    }
    var toSend = [];

    for (var x = 0; x < doctor.length; x++) {
        myDoctor = doctor[x];
        myAdd = myDoctor['addresses'][0];
        tempJSON = {
            id:myDoctor['_id'],
            name: myDoctor['name'],
            speciality: myDoctor['speciality'],
            addresses: myDoctor['addresses'],
            bio: myDoctor['bio'],
            qualifications:myDoctor['qualifications']
        }
        toSend.push(tempJSON);
        // console.log(toSend);
    }
    res.status(201).json(toSend);
}

exports.consultDoctor = async function (req,res,next) {
    // console.log(req);
    // console.log((req.body))
    // console.log(req.body);
    var patient = await Patient.findById(req.user.id);
    var doctor = await Doctor.findById(req.body._id);
    var notifDoctor = doctor['notifToken'][0];
    var patientName = patient['name'];
    var patientContact = patient['contactNumber'];
    var status = "Pending";
    var doctorName = doctor['name'];
    var clinicAddress = req.body.address;
    var doctorContact = doctor['contactNumber'];
    var time = req.body.time;
    var date = req.body.date;
    var reasonForVisit = req.body.reasonForVisit;
    var json = {
        patientId:patient['_id'],
        doctorId:doctor['_id'],
        patientName:patientName,
        patientContact:patientContact,
        status:status,
        doctorName:doctorName,
        doctorSpeciality: doctor['speciality'],
        clinicAddress:clinicAddress,
        doctorContact:doctorContact,
        time:time,
        date:date,
        reasonForVisit:reasonForVisit
    }
    var appointment = new Appointment(json);
    var appointment = await appointment.save();
    (patient['appointment']).push(appointment['_id']);
    (doctor['appointment']).push(appointment['_id']);
    var p = await patient.save();
    var d = await doctor.save();

    // Sending Message

    var message = {
        to: notifDoctor,
        collapse_key: 'cowapsemesenpai',
        notification: {
            title: 'You have an appointment!',
            body: 'Check it!'
        },
    };

    fcmController.fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!" + err);
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
    res.status(201).json(appointment);
}

exports.getAppointments = async function (req,res,next){
    var patient = await Patient.findById(req.user.id);
    var appointments = patient['appointment'];
    var history = patient['history'];
    var cancelledApp = patient['cancelledAppointment'];
    var toSend = [];
    console.log("Here");
    for(var x = 0;  x < appointments.length; x++){
        var tempApp = await Appointment.findById(appointments[x]);
        var tempJSON = {
            appointmentId: tempApp['_id'],
            doctorId: tempApp['doctorId'],
            clinicAddress: tempApp['clinicAddress'],
            status : tempApp['status'],
            doctorName : tempApp['doctorName'],
            time: tempApp['time'],
            date: tempApp['date'],
            doctorSpeciality: tempApp['doctorSpeciality'],
            reasonForVisit : tempApp['reasonForVisit']
        }
        toSend.push(tempJSON)
    }
    for(var x = 0; x < history.length; x++){
        var tempApp = await History.findById(history[x]);
        var tempJSON = {
            appointmentId: tempApp['_id'],
            doctorId: tempApp['doctorId'],
            clinicAddress: tempApp['clinicAddress'],
            status : tempApp['status'],
            doctorName : tempApp['doctorName'],
            time: tempApp['time'],
            date: tempApp['date'],
            doctorSpeciality: tempApp['doctorSpeciality'],
            reasonForVisit : tempApp['reasonForVisit']
        }
        toSend.push(tempJSON)
    }
    for(var x = 0; x < cancelledApp.length; x++){
        var tempApp = await CancelledAppointment.findById(cancelledApp[x]);
        var tempJSON = {
            appointmentId: tempApp['_id'],
            doctorId: tempApp['doctorId'],
            clinicAddress: tempApp['clinicAddress'],
            status : tempApp['status'],
            doctorName : tempApp['doctorName'],
            time: tempApp['time'],
            date: tempApp['date'],
            doctorSpeciality: tempApp['doctorSpeciality'],
            reasonForVisit : tempApp['reasonForVisit']
        }
        toSend.push(tempJSON)
    }

    res.status(201).send(toSend);
}

exports.cancelAppointments = async function (req,res,next){
    var appointment = await Appointment.findById(req.params.appointmentId);
    // Make it history
    var cancelledAppointment = {
        patientId: appointment["patientId"],
        doctorId: appointment["doctorId"],
        patientName: appointment["patientName"],
        patientContact: appointment["patientContact"],
        status: appointment["status"],
        doctorName: appointment["doctorName"],
        doctorContact: appointment["doctorContact"],
        doctorSpeciality: appointment["doctorSpeciality"],
        clinicAddress: appointment["clinicAddress"],
        time: appointment["time"],
        date: appointment["date"],
        reasonForVisit: appointment["reasonForVisit"],
        status: "Cancelled" // ENUM-ify
    }
   
    var cancelledApp = new CancelledAppointment(cancelledAppointment);

    await cancelledApp.save();

    var patient = await Patient.findById(appointment['patientId']);
    patientAppointments = patient['appointment']
    var doctor = await Doctor.findById(appointment['doctorId']);
    doctorAppointments = doctor['appointment']
    patientAppointments.splice(patientAppointments.indexOf(appointment['_id']), 1);
    doctorAppointments.splice(doctorAppointments.indexOf(appointment['_id']), 1);
    patient['cancelledAppointment'].push(cancelledApp['_id']);
    doctor['cancelledAppointment'].push(cancelledApp['_id']);
    await patient.save();
    await doctor.save();
    await Appointment.findByIdAndRemove(appointment['_id']);
    console.log(cancelledApp);
    res.status(201).json(cancelledApp);
}

exports.getPrescriptions = async function (req,res,next){
    var patient = await Patient.findById(req.user.id);
    var prescriptions = patient['prescriptions'];
    var toSend = []
    for(var x = 0; x < prescriptions.length; x++){
        var myPrescription = await Prescription.findById(prescriptions[x]);
        toSend.push(myPrescription);
    }
    res.status(201).send(toSend);
}

exports.getHistory = async function (req,res,next){
    var patient = await Patient.findById(req.user.id);
    var history = patient['history'];
    var toSend = [];
    for(var x = 0; x < 6; x++){
        var visit = await History.findById(history[x]);
        if(visit==null){
            break;
        }
        toSend.push(visit)
    }
    res.status(201).send(toSend);
}