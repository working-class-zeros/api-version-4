var jwt = require('jsonwebtoken'); 
var Patient = require('../models/patient.model');
var Doctor = require('../models/doctor.model');
var authConfig = require('../../config/auth');
 
function generateToken(Patient){
    return jwt.sign(Patient, authConfig.secret, {
        expiresIn: 10080
    });
}
 
function setInfo(request){
    return {
        _id: request._id,
        email: request.email,
    };
}
 

exports.login = function(req, res, next){
 
    var patientInfo = setInfo(req.user);
 
    res.status(200).json({
        token: 'JWT ' + generateToken(patientInfo),
        patient: patientInfo
    });
 
}
 
exports.register = function(req, res, next){
 
    var email = req.body.email;
    var password = req.body.password;
    var name = req.body.name;
    var dob = req.body.dob;
    var address = req.body.address;
    var contactNumber = req.body.contactNumber
    var notifToken = req.body.notifToken;

 
    if(!email){
        return res.status(422).send({error: 'You must enter an email address'});
    }
 
    if(!password){
        return res.status(422).send({error: 'You must enter a password'});
    }
 
    Patient.findOne({email: email}, function(err, existingPatient){
 
        if(err){
            return next(err);
        }
 
        if(existingPatient){
            return res.status(422).send({error: 'That email address is already in use'});
        }
 
        var patient = new Patient({
            email: email,
            password: password,
            name:name,
            dob:dob,
            address:address,
            contactNumber:contactNumber,
            notifToken:notifToken
        });
        console.log("Saving Patient")
        console.log(patient)
 
        patient.save(function(err, patient){
 
            if(err){
                return next(err);
            }
 
            var patientInfo = setInfo(patient);
 
            res.status(201).json({
                token: 'JWT ' + generateToken(patientInfo),
                patient: patientInfo
            })
 
        });
 
    });
}


// Doctor Authentication Routes

exports.doctorRegister = function(req, res, next){
 
    var email = req.body.email;
    var password = req.body.password;
    var name = req.body.name;
    var dob = req.body.dob;
    var rating = 0;
    var addresses = req.body.addresses;
    var qualifications = req.body.qualifications;
    var speciality = req.body.speciality;
    var bio = req.body.bio;
    var contactNumber = req.body.contactNumber;
    var notifToken = req.body.notifToken;
    
    if(!email){
        return res.status(422).send({error: 'You must enter an email address'});
    }
 
    if(!password){
        return res.status(422).send({error: 'You must enter a password'});
    }
 
    Doctor.findOne({email: email}, function(err, existingPatient){
 
        if(err){
            return next(err);
        }
 
        if(existingPatient){
            return res.status(422).send({error: 'That email address is already in use'});
        }
 
        var doctor = new Doctor({
            email: email,
            password: password,
            name: name,
            dob:dob,
            rating:rating,
            addresses:addresses,
            qualifications:qualifications,
            speciality:speciality,
            bio:bio,
            notifToken:notifToken,
            contactNumber: contactNumber
        });
 
        doctor.save(function(err, doctor){
 
            if(err){
                return next(err);
            }
 
            var doctorInfo = setInfo(doctor);
 
            res.status(201).json({
                token: 'JWT ' + generateToken(doctorInfo),
                doctor: doctorInfo
            })
 
        });
 
    });
}

 

exports.doctorLogin = function(req, res, next){
 
    var doctorInfo = setInfo(req.user);
 
    res.status(200).json({
        token: 'JWT ' + generateToken(doctorInfo),
        doctor: doctorInfo
    });
 
}

//  Dummy

exports.dummy = async function(req,res,next){
    var patient = await Patient.findOne({name:"Rohan"});
    res.status(201).json(patient);
}

exports.postDummy = function(req,res,next){
    console.log(req);
    res.status(201).json({"content":"DONE"});
}