var Doctor = require("../models/doctor.model");
var Patient = require("../models/patient.model");
var Appointment = require('../models/appointment.model');
var History = require('../models/history.model');
var Prescription = require('../models/prescription.model');
var CancelledAppointment = require('../models/appointment-cancel.model');
exports.searchUser = async function (req, res, next) {
    var patientName = req.params.name;
    patientName = patientName.replace("+", " ");
    patientName = patientName.toLowerCase();
    var patient = await Patient.find({
        name: patientName
    });
    // console.log(patient);
    if (patient.length == 0) {
        res.json({
            status: 404
        }).status(404);
        return;
    }
    var toSend = [];
    for (var x = 0; x < patient.length; x++) {
        myPatient = patient[x];
        var myJSON = {
            id: myPatient['_id'],
            name: myPatient['name'],
            age: myPatient['age'],
            dob: myPatient['dob']
        }
        toSend.push(myJSON);
        // console.log(toSend);
    }
    res.status(201).json(toSend);
}

exports.getPendingAppointments = async function (req, res, next) {
    // console.log(req.user);
    var doctor = await Doctor.findById(req.user.id);
    var appointments = doctor['appointment'];
    var toReturn = [];
    for (var x = 0; x < appointments.length; x++) {
        var appointment = await Appointment.findById(appointments[x]);
        if (appointment['status'] === "Pending") { // ENUM-IFY
            var tempAppointment = {
                id: appointment['_id'],
                patientName: appointment['patientName'],
                patientContact: appointment['patientContact'],
                clinicAddress: appointment['clinicAddress'],
                time: appointment['time'],
                date: appointment['date'],
                reasonForVisit: appointment['reasonForVisit']
            };
            toReturn.push(tempAppointment);
        }
    }
    res.status(201).json(toReturn);
}

exports.rejectAppointment = async function (req, res, next) {
    var appId = req.params.appId;

    var appointment = await Appointment.findById(appId);
    // appointment['status'] = "Cancelled"; //ENUM-IFY
    // await appointment.save();
    // var historyAppointment = appointment;
    var cancelledAppointment = {
        patientId: appointment["patientId"],
        doctorId: appointment["doctorId"],
        patientName: appointment["patientName"],
        patientContact: appointment["patientContact"],
        status: appointment["status"],
        doctorName: appointment["doctorName"],
        doctorContact: appointment["doctorContact"],
        doctorSpeciality: appointment["doctorSpeciality"],
        clinicAddress: appointment["clinicAddress"],
        time: appointment["time"],
        date: appointment["date"],
        reasonForVisit: appointment["reasonForVisit"],
        status: "Cancelled" // ENUM-ify
    }

    var cancelledApp = new CancelledAppointment(cancelledAppointment);

    await cancelledApp.save();


    var patient = await Patient.findById(appointment['patientId']);
    patientAppointments = patient['appointment']
    var doctor = await Doctor.findById(appointment['doctorId']);
    doctorAppointments = doctor['appointment']
    patientAppointments.splice(patientAppointments.indexOf(appointment['_id']), 1);
    doctorAppointments.splice(doctorAppointments.indexOf(appointment['_id']), 1);
    patient['cancelledAppointment'].push(cancelledApp['_id']);
    doctor['cancelledAppointment'].push(cancelledApp['_id']);
    await patient.save();
    await doctor.save();
    await Appointment.findByIdAndRemove(appointment['_id']);
    console.log(cancelledApp);
    res.status(201).json(cancelledApp);
}

exports.acceptAppointment = async function (req, res, next) {
    var appId = req.params.appId;
    var appointment = await Appointment.findById(appId);
    appointment['status'] = "Accepted"; //ENUM-IFY
    await appointment.save();
    res.status(201).json(appointment);
}

exports.getSchedule = async function (req, res, next) {
    var docId = req.user.id;
    var doctor = await Doctor.findById(docId);
    var appointments = doctor['appointment'];
    var toSend = [];
    for (var x = 0; x < appointments.length; x++) {
        var oneAppointment = await Appointment.findById(appointments[x]['_id'])
        if (oneAppointment['status'] == "Accepted") { // ENUM-ify
            toSend.push(oneAppointment);
        }
    }
    res.status(201).send(toSend);
}

exports.makePrescription = async function (req, res, next) {
    var docId = req.user.id;
    var appId = req.params.appId;
    var doctor = await Doctor.findById(docId);
    var appointment = await Appointment.findById(appId);
    var patient = await Patient.findById(appointment['patientId']);
    var prescription = {
        patientId: appointment["patientId"],
        doctorId: docId,
        doctorName: doctor['name'],
        doctorQualification: doctor['qualifications'],
        doctorContact: doctor['contactNumber'],
        clinicAddress: appointment['clinicAddress'],
        patientName: patient['name'],
        patientDOB: patient['dob'],
        date: appointment['date'],
        medicines: req.body.medicines,
        tests: req.body.tests,
        status: "Created" //ENUM-IFY
    }

    var newPrescription = await new Prescription(prescription);
    // console.log("Prescription Made");
    // console.log(newPrescription)
    await newPrescription.save();

    var historyAppointment = {
        patientId: appointment["patientId"],
        doctorId: appointment["doctorId"],
        patientName: appointment["patientName"],
        patientContact: appointment["patientContact"],
        status: appointment["status"],
        doctorName: appointment["doctorName"],
        doctorContact: appointment["doctorContact"],
        doctorSpeciality: appointment["doctorSpeciality"],
        clinicAddress: appointment["clinicAddress"],
        time: appointment["time"],
        date: appointment["date"],
        reasonForVisit: appointment["reasonForVisit"],
        status: "Done" // ENUM-ify
    }

    var historyApp = new History(historyAppointment);

    await historyApp.save();

    patientAppointments = patient['appointment']
    doctorAppointments = doctor['appointment']
    patientAppointments.splice(patientAppointments.indexOf(appointment['_id']), 1);
    doctorAppointments.splice(doctorAppointments.indexOf(appointment['_id']), 1);
    patient['history'].push(historyApp['_id']);
    doctor['history'].push(historyApp['_id']);
    patient['prescriptions'].push(newPrescription['_id']);
    await patient.save();
    await doctor.save();
    await Appointment.findByIdAndRemove(appointment['_id']);
    console.log(cancelledApp);
    res.status(201).send(newPrescription);
}

exports.getHistory = async function (req, res, next) {
    var docId = req.user.id;
    var doctor = await Doctor.findById(req.user.id);
    var appointments = doctor['history'];
    var cancelled = doctor['cancelledAppointment']
    var toSend = [];
    for (var x = 0; x < appointments.length; x++) {
        var appointment = await History.findById(appointments[x]);
        if (appointment['status'] != "Pending") { // ENMUM-IFY
            toSend.push(appointment);
        }
    }
    for (var x = 0; x < cancelled.length; x++) {
        var appointment = await CancelledAppointment.findById(cancelled[x]);
        toSend.push(appointment);
    }
    res.status(201).json(toSend);
}