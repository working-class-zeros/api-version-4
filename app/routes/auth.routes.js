var authController = require('../controllers/auth.controller'),
    express = require('express'),
    passportService = require('../../config/auth');
    passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
var requireDoctorLogin = passport.authenticate('dr-local-login',{session:false});
var requireDoctorAuth = passport.authenticate('jwt-doctor',{session: false});

module.exports = (app) => {
   app.post('/patient/register',authController.register);
   app.post('/patient/login',requireLogin,authController.login);
   app.get('/protec', requireAuth, function(req,res){
       res.send({content: "SUCCESS"});
   });

   app.get('/protecDoctor', requireDoctorAuth, function(req,res){
    res.send({content: "SUCCESS"});
});

   app.post('/doctor/register', authController.doctorRegister);
   app.post('/doctor/login',requireDoctorLogin, authController.doctorLogin);


//    test routes

   app.get('/dummy',authController.dummy);
   app.post('/dummy',authController.postDummy);

   

}