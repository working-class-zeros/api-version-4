## v0.0.1 -> v0.0.2

*   Successful Generation of Javascript Web Token!
*   User account creation
*   Login works!

## v0.0.2 -> v0.0.3

*   Added models for doctor
*   Separated Logins for User, Doctor.
*   Separated previousVisit and appointment component to be shareable between doctor & patient
*   Tested Data sending capabilities through POST to send Patient-And-Doctor info via Post Routes! (Spoiler : It **works** \m/)

## v0.0.3 -> v0.0.4

*   Cleaned all testing routes
*   Function for patient to search doctor by name added. Returns all matches if multiple doctors with same name present or 404 if none
*   Cleaned doctor model, added hashing and minor changes to Doctor and Patient model.

## v0.0.4 -> v0.0.5

* Patient can now search doctors without case-sensitivity
* Doctor schema changed to accomodate bio, rating, average fees, etc.
* Return from search is changed and sends more data than before
* Signing up a doctor will take in all required data instead of just username and password

## v0.0.5 -> v0.0.6

* Added search functionality for doctor to search patient (might be removed in the future)
* Improved patient signup, takes in more details
* Created Review Model
* Fixed bugs in Prescription Model
* Renamed PreviousVisit model to History Model because PreviousVisit is history (duh)
* Updated MongooseJS version (4.x.x -> 5.2.x) to fix $pushAll conflicts/errors.
* Added route to send all appointment details in Patient.routes. 

## v0.0.6 -> v0.0.7

* Minor changes in appointment model to include additional important fields
* Patient route to cancel an appointment written
* Patient route to create an appointment written

## v0.0.7 -> v0.0.8

*  Added Doctor Route to get pending appointments
*  Added doctor route to accept or reject appointments
*  

## v0.0.8 -> v0.0.9

* Cancelled appointment will now go to History collection, so Appointment collection wil only have approved or pending appointments
    *   //TODO : Further, an option to classify an appointment as "done" would be given after session with the doctor is over.
* History model re-written to be consistent with Appointment model. [History Model = Appointment Model + PrescriptionID]
* Retrieval of appointments for patient will now also get History appointments
* Wrote routes to get schedule for doctor
* Route to get prescriptions of a particular patient
* Made changes to History model to add status,
* Completed addition of prescription route.
* Once prescription is added, the appointment status is changed to "DONE" and goes to history collection
* Wrote and tested route to retrieve all prescription pertaining to a particular patient.

## v0.0.9 -> v0.0.10

*   Re-wrote doctor signup route
*   Wrote doctor getHistory route
*   Fixed appointment Cancellation bug (#1)
*   Fixed doctor not-in-session bug. (#2)
*   Added route to get patient history (TODO : Get top 5 latest history, fix consult page)
*   Created collection for Cancelled appointments. On rejection of appointment by patient or doctor, a new record is created in a cancelled appointment collection instead of history.
*   Fixed basic errors in models (Spelling, Types)
* Added Reason Field in prescription.model

## v0.0.10 -> v0.1.0

* Trying to add Notification support from backend using Firebase Cloud Messaging.
* Integrated NotifToken in model. Should push notif wherever necessary

## v0.1.0 -> 0.1.1

* Notification support works for Patient-Doctor app. When Patient makes an appointment, doctor has a notification