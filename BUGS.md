# Bugs

## 1. Appointment Cancellation

Cancelled appointments would go to History. This is bad design as History should be a collection of previous, successful appointments. 

Solution : Create a new collection for cancelled appointments

## 2. Doctor in-session bug

Doctor session would not be stored. For example, if the doctor logged in a JWT token would be assigned to the doctor. On page refresh, the token is still valid and the doctor must not be asked to login again. But, it would as for sign up again.

Reason :

This was because whenever we find a token stored in the doctor app, we authenticate that token against a url `/protec`. This URL is specific to the patient and uses the JWT authentication module written for the patient.

Solution :

A separate URL was created called `/protecDoctor` which uses the JWT authentication module written for the doctor